package org.alexz.app.imdb_app.viper.data.logic.api

import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.data.entity.SearchResult
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface IMDBApiService {
    companion object {
        private const val URL = "http://www.omdbapi.com/"
        private const val API_KEY = "8de36961"

        val service = Retrofit.Builder()
                .baseUrl(URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                        OkHttpClient.Builder()
                                .addInterceptor(
                                        HttpLoggingInterceptor()
                                                .setLevel(
                                                        HttpLoggingInterceptor.Level.BODY
                                                )
                                )
                                .build()
                )
                .build()
                .create(IMDBApiService::class.java)

    }

    @GET("?apikey=$API_KEY&plot=full")
    fun getFilmBy(@Query("i") imdbID: String): Single<Film>

    @GET("?apikey=$API_KEY&plot=full")
    fun getFilmSearchBy(@Query("s") word: String): Single<SearchResult>
}