package org.alexz.app.imdb_app.lang.arch.rxviper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.reactivestreams.Subscriber;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

import static io.reactivex.internal.functions.Functions.EMPTY_ACTION;
import static io.reactivex.internal.functions.Functions.ERROR_CONSUMER;
import static org.alexz.app.imdb_app.lang.arch.rxviper.RxViper.requireNotNull;

public abstract class Interactor<RequestModel, ResponseModel> implements Disposable {
    @NonNull
    private final Scheduler subscribeScheduler;
    @NonNull
    private final Scheduler observeScheduler;
    @NonNull
    private final CompositeDisposable disposables;

    protected Interactor(@NonNull Scheduler subscribeScheduler, @NonNull Scheduler observeScheduler) {
        requireNotNull(subscribeScheduler);
        requireNotNull(observeScheduler);

        this.subscribeScheduler = subscribeScheduler;
        this.observeScheduler = observeScheduler;
        disposables = new CompositeDisposable();
    }

    public final void execute(@NonNull Consumer<? super ResponseModel> onNext) {
        execute(onNext, (RequestModel) null);
    }

    public final void execute(@NonNull Consumer<? super ResponseModel> onNext, @Nullable RequestModel requestModel) {
        execute(onNext, ERROR_CONSUMER, EMPTY_ACTION, requestModel);
    }

    public final void execute(@NonNull Consumer<? super ResponseModel> onNext, @NonNull Consumer<Throwable> onError) {
        execute(onNext, onError, (RequestModel) null);
    }

    public final void execute(@NonNull Consumer<? super ResponseModel> onNext, @NonNull Consumer<Throwable> onError,
                              @Nullable RequestModel requestModel) {
        execute(onNext, onError, EMPTY_ACTION, requestModel);
    }

    public final void execute(@NonNull Consumer<? super ResponseModel> onNext, @NonNull Consumer<Throwable> onError,
                              @NonNull Action onComplete) {
        execute(onNext, onError, onComplete, null);
    }

    public final void execute(@NonNull Consumer<? super ResponseModel> onNext, @NonNull Consumer<Throwable> onError,
                              @NonNull Action onComplete, @Nullable RequestModel requestModel) {
        requireNotNull(onNext);
        requireNotNull(onError);
        requireNotNull(onComplete);
        disposables.add(flowable(requestModel).subscribe(onNext, onError, onComplete));
    }

    final void execute(@NonNull Subscriber<? super ResponseModel> subscriber) {
        execute(subscriber, null);
    }

    public final void execute(@NonNull Subscriber<? super ResponseModel> subscriber, @Nullable RequestModel requestModel) {
        requireNotNull(subscriber);

        final Flowable<ResponseModel> f = flowable(requestModel);
        disposables.add(subscriber instanceof Disposable ? (Disposable) f.subscribeWith(subscriber)
                : f.subscribeWith(new RxViperSubscriber<>(subscriber)));
    }

    @Override
    public final void dispose() {
        // call clear() instead of dispose() to be able to manage new disposables
        disposables.clear();
    }

    @Override
    public final boolean isDisposed() {
        return disposables.size() == 0;
    }

    @NonNull
    protected abstract Flowable<ResponseModel> createFlowable(@Nullable RequestModel requestModel);

    @NonNull
    private Flowable<ResponseModel> flowable(@Nullable RequestModel requestModel) {
        return createFlowable(requestModel).subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler);
    }
}
