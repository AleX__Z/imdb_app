package org.alexz.app.imdb_app.viper.presentation.logic.presenter.film_search

import android.arch.lifecycle.ViewModel
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.domain.logic.film_search.FilmSearchInteractor
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.BaseAppPresenter
import org.alexz.app.imdb_app.viper.presentation.logic.view.film_search.FilmSearchView

class FilmSearchPresenter(
        private val filmSearchInteractor: FilmSearchInteractor
) : BaseAppPresenter<FilmSearchView>() {
    override fun onStart() {
        view.showContent()
    }

    override fun onStop() {
        filmSearchInteractor.dispose()
    }

    fun searchFilm(word: String) {
        view.showProgress()

        filmSearchInteractor.execute(
                {
                    with(view) {
                        showContent()
                        showFilmSearch(it)
                    }

                },
                {
                    with(view) {
                        showContent()
                        showError("Unknown error")
                    }
                },
                word
        )
    }

    fun showFilmDetail(filmPreview: FilmPreview) {
        view.showProgress()
        router.showFilmDetail(filmPreview)
    }


}