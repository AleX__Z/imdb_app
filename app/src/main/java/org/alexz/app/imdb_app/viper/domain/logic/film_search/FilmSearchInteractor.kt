package org.alexz.app.imdb_app.viper.domain.logic.film_search

import io.reactivex.Flowable
import io.reactivex.Scheduler
import org.alexz.app.imdb_app.lang.arch.rxviper.Interactor
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.data.logic.film_search.FilmSearchProvider

class FilmSearchInteractor(
        subscribeScheduler: Scheduler,
        observeScheduler: Scheduler,
        private val filmSearchProvider: FilmSearchProvider
) : Interactor<String, List<FilmPreview>>(subscribeScheduler, observeScheduler) {

    override fun createFlowable(requestModel: String?): Flowable<List<FilmPreview>> =
            filmSearchProvider.getFilmSearchBy(requestModel as String).toFlowable()

}