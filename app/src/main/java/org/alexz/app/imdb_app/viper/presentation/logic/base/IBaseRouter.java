package org.alexz.app.imdb_app.viper.presentation.logic.base;

import org.alexz.app.imdb_app.lang.arch.rxviper.Router;

public interface IBaseRouter extends Router {
    void onStartRouter();
}
