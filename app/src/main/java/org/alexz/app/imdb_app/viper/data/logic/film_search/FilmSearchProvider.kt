package org.alexz.app.imdb_app.viper.data.logic.film_search

import io.reactivex.Single
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview

interface FilmSearchProvider {
    fun getFilmSearchBy(word: String): Single<List<FilmPreview>>
}