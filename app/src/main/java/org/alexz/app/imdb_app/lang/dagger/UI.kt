package org.alexz.app.imdb_app.lang.dagger

import java.lang.annotation.Documented
import java.lang.annotation.Retention

import javax.inject.Qualifier

import java.lang.annotation.RetentionPolicy.RUNTIME

@Qualifier
@Documented
@Retention(RUNTIME)
annotation class UI
