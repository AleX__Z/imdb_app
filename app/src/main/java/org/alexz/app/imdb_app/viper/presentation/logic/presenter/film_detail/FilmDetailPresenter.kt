package org.alexz.app.imdb_app.viper.presentation.logic.presenter.film_detail

import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.domain.logic.film_detail.FilmDetailInteractor
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.BaseAppPresenter
import org.alexz.app.imdb_app.viper.presentation.logic.view.film_detail.FilmDetailView

class FilmDetailPresenter(
        private val filmDetailInteractor: FilmDetailInteractor
) : BaseAppPresenter<FilmDetailView>() {
    lateinit var filmPreview: FilmPreview
    lateinit var film: Film

    override fun onStart() {
        view.showProgress()

        filmDetailInteractor.execute({
            film = it

            view.showContent()
            view.showFilmDetail(film)

        }, filmPreview.imdbID)
    }

    override fun onStop() {
        filmDetailInteractor.dispose()
    }

}