package org.alexz.app.imdb_app.viper.presentation.logic.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.alexz.app.imdb_app.App;
import org.alexz.app.imdb_app.lang.arch.rxviper.Router;
import org.alexz.app.imdb_app.lang.arch.rxviper.ViewCallbacks;
import org.alexz.app.imdb_app.AppComponent;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements ViewCallbacks {
    private static final AtomicInteger lastFragmentId = new AtomicInteger(0);
    private final int fragmentId;

    public BaseFragment() {
        fragmentId = lastFragmentId.incrementAndGet();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        //setRetainInstance(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializationView();
    }

    protected void initializationView() {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inject();
        //noinspection unchecked
        getPresenter().takeView(this);
        getPresenter().takeRouter(getRouter());

        initializationPresenter(getPresenter());
    }

    protected void initializationPresenter(T presenter) {

    }

    //TODO КОСТЫЛЬ. Router должен проявиться из DI (Dagger). ЗАРЭЖУ.
    private Router getRouter() {
        return (Router) getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onStop();
    }

    @Override
    public void onDestroyView() {
        getPresenter().dropRouter(getRouter());
        super.onDestroyView();
    }

    public String getFragmentName() {
        return Long.toString(fragmentId);
    }

    @NonNull
    protected abstract T getPresenter();

    public AppComponent getDaggerAppComponent() {
        return ((App) getActivity().getApplication()).getAppComponent();
    }

    public abstract void inject();

    @LayoutRes
    protected abstract int getLayoutRes();
}