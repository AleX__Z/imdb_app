package org.alexz.app.imdb_app.viper.data.entity

import com.google.gson.annotations.SerializedName

data class SearchResult(
        @SerializedName("Search")
        val listFilmPreview: List<FilmPreview>
)