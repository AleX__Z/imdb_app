package org.alexz.app.imdb_app.lang.arch.rxviper;

import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class NullObject<T> implements InvocationHandler {
    private WeakReference<T> targetRef;

    NullObject(@Nullable T target) {
        set(target);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final T target = get();
        if (target == null) {
            return null;
        } else {
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            return method.invoke(target, args);
        }
    }

    void clear() {
        if (targetRef != null) {
            targetRef.clear();
            targetRef = null;
        }
    }

    @Nullable
    T get() {
        return targetRef == null ? null : targetRef.get();
    }

    void set(@Nullable T target) {
        clear();
        targetRef = target == null ? null : new WeakReference<>(target);
    }
}
