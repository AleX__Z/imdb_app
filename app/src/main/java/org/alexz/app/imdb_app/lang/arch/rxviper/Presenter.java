package org.alexz.app.imdb_app.lang.arch.rxviper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static org.alexz.app.imdb_app.lang.arch.rxviper.RxViper.getProxy;
import static org.alexz.app.imdb_app.lang.arch.rxviper.RxViper.requireNotNull;


public abstract class Presenter<V extends ViewCallbacks> {
    @NonNull
    private final V viewProxy = RxViper.createView(null, getClass());

    protected Presenter(@NonNull V view) {
        requireNotNull(view);
        getProxy(viewProxy).set(view);
    }

    protected Presenter() {
    }

    public final void dropView(@NonNull V view) {
        requireNotNull(view);

        if (currentView() == view) {
            onDropView(view);
            getProxy(viewProxy).clear();
        }
    }

    public final boolean hasView() {
        return currentView() != null;
    }

    public final void takeView(@NonNull V view) {
        requireNotNull(view);

        final V currentView = currentView();
        if (currentView != view) {
            if (currentView != null) {
                dropView(currentView);
            }
            getProxy(viewProxy).set(view);
            onTakeView(view);
        }
    }

    @NonNull
    protected final V getView() {
        return viewProxy;
    }

    protected void onDropView(@NonNull V view) {
    }

    protected void onTakeView(@NonNull V view) {
    }

    @Nullable
    private V currentView() {
        return getProxy(viewProxy).get();
    }
}
