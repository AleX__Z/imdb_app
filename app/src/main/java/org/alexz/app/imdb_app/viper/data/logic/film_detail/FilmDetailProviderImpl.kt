package org.alexz.app.imdb_app.viper.data.logic.film_detail

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.data.logic.api.IMDBApiService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

class FilmDetailProviderImpl : FilmDetailProvider {
    override fun provideFilmBy(imdbID: String): Single<Film> =
            IMDBApiService.service.getFilmBy(imdbID)

}