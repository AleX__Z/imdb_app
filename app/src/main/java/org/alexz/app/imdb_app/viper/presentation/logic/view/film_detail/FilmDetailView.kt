package org.alexz.app.imdb_app.viper.presentation.logic.view.film_detail

import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.presentation.logic.view.IAppView

interface FilmDetailView : IAppView {
    fun showFilmDetail(film: Film)
}