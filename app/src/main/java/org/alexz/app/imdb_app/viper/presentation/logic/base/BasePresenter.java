package org.alexz.app.imdb_app.viper.presentation.logic.base;


import org.alexz.app.imdb_app.lang.arch.rxviper.Router;
import org.alexz.app.imdb_app.lang.arch.rxviper.ViewCallbacks;
import org.alexz.app.imdb_app.lang.arch.rxviper.ViperPresenter;

public abstract class BasePresenter<V extends ViewCallbacks, R extends Router>
        extends ViperPresenter<V, R> {
    public abstract void onStart();

    public abstract void onStop();
}
