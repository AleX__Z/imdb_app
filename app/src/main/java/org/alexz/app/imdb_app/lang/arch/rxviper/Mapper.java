package org.alexz.app.imdb_app.lang.arch.rxviper;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;

import io.reactivex.functions.Function;

import static org.alexz.app.imdb_app.lang.arch.rxviper.RxViper.requireNotNull;


public abstract class Mapper<From, To> implements Function<From, To> {
    /* @since 0.10.0 */
    @Override
    public final To apply(@NonNull From from) {
        requireNotNull(from);
        return map(from);
    }

    public Collection<To> map(@NonNull Collection<From> entities) {
        requireNotNull(entities);
        final Collection<To> result = new ArrayList<>(entities.size());
        //noinspection Convert2streamapi
        for (From from : entities) {
            result.add(map(from));
        }
        return result;
    }

    public abstract To map(From entity);
}
