package org.alexz.app.imdb_app.lang.arch.rxviper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static org.alexz.app.imdb_app.lang.arch.rxviper.RxViper.getProxy;
import static org.alexz.app.imdb_app.lang.arch.rxviper.RxViper.requireNotNull;


public abstract class ViperPresenter<V extends ViewCallbacks, R extends Router> extends Presenter<V> {
    @NonNull
    private final R routerProxy = RxViper.createRouter(null, getClass());

    protected ViperPresenter(@NonNull V view, @NonNull R router) {
        super(view);
        requireNotNull(router);
        getProxy(routerProxy).set(router);
    }

    protected ViperPresenter(@NonNull V view) {
        super(view);
    }

    protected ViperPresenter(@NonNull R router) {
        requireNotNull(router);
        getProxy(routerProxy).set(router);
    }

    protected ViperPresenter() {
    }

    public final void dropRouter(@NonNull R router) {
        requireNotNull(router);

        if (currentRouter() == router) {
            onDropRouter(router);
            getProxy(routerProxy).clear();
        }
    }

    public final boolean hasRouter() {
        return currentRouter() != null;
    }

    public final void takeRouter(@NonNull R router) {
        requireNotNull(router);

        final R currentRouter = currentRouter();
        if (currentRouter != router) {
            if (currentRouter != null) {
                dropRouter(currentRouter);
            }
            getProxy(routerProxy).set(router);
            onTakeRouter(router);
        }
    }

    @NonNull
    protected final R getRouter() {
        return routerProxy;
    }

    protected void onDropRouter(@NonNull R router) {
    }

    protected void onTakeRouter(@NonNull R router) {
    }

    @Nullable
    private R currentRouter() {
        return getProxy(routerProxy).get();
    }
}
