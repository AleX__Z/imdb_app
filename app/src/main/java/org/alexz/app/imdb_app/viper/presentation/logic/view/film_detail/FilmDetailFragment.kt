package org.alexz.app.imdb_app.viper.presentation.logic.view.film_detail

import android.os.Bundle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_film_detail.*
import org.alexz.app.imdb_app.R
import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.film_detail.FilmDetailPresenter
import org.alexz.app.imdb_app.viper.presentation.logic.view.BaseAppFragment
import javax.inject.Inject

class FilmDetailFragment : BaseAppFragment<FilmDetailPresenter>(), FilmDetailView {
    companion object {
        private const val KEY_FILM_PREVIEW = "KEY_FILM_PREVIEW"

        fun newInstance(filmPreview: FilmPreview): FilmDetailFragment =
                FilmDetailFragment().apply {
                    arguments = Bundle().apply { putParcelable(KEY_FILM_PREVIEW, filmPreview) }
                }

    }

    @Inject
    lateinit var filmDetailPresenter: FilmDetailPresenter

    override fun getPresenter(): FilmDetailPresenter = filmDetailPresenter

    override fun inject() = daggerAppComponent.inject(this)

    override fun getLayoutRes(): Int = R.layout.fragment_film_detail

    override fun initializationPresenter(presenter: FilmDetailPresenter) {
        presenter.filmPreview = arguments!!.getParcelable(KEY_FILM_PREVIEW) as FilmPreview
    }

    override fun showFilmDetail(film: Film) {
        with(film) {
            nameTextView.text = name
            ratingTextView.text = rating.toString()
            countryTextView.text = country
            directorTextView.text = directorName
            actorsTextView.text = actorNames
            descriptionTextView.text = description

            Picasso.get().load(imageUrl).into(posterImageView)
        }
    }
}