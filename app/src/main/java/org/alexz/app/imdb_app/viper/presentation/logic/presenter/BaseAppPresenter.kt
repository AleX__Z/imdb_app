package org.alexz.app.imdb_app.viper.presentation.logic.presenter

import org.alexz.app.imdb_app.viper.presentation.logic.base.BasePresenter
import org.alexz.app.imdb_app.viper.presentation.logic.router.IAppRouter
import org.alexz.app.imdb_app.viper.presentation.logic.view.IAppView

abstract class BaseAppPresenter<V : IAppView> : BasePresenter<V, IAppRouter>()