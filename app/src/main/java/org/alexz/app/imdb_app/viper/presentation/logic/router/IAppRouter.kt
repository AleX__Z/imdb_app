package org.alexz.app.imdb_app.viper.presentation.logic.router

import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.presentation.logic.base.IBaseRouter

interface IAppRouter : IBaseRouter {
    fun showSearchFilm()

    fun showFilmDetail(filmPreview: FilmPreview)
}