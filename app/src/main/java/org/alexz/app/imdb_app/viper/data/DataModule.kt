package org.alexz.app.imdb_app.viper.data

import dagger.Module
import dagger.Provides
import org.alexz.app.imdb_app.viper.data.logic.film_detail.FilmDetailProvider
import org.alexz.app.imdb_app.viper.data.logic.film_detail.FilmDetailProviderImpl
import org.alexz.app.imdb_app.viper.data.logic.film_search.FilmSearchProvider
import org.alexz.app.imdb_app.viper.data.logic.film_search.FilmSearchProviderImpl

@Module
class DataModule {
    @Provides
    fun provideFilmProvider(): FilmDetailProvider = FilmDetailProviderImpl()

    @Provides
    fun provideFilmSearchProveder(): FilmSearchProvider = FilmSearchProviderImpl()
}