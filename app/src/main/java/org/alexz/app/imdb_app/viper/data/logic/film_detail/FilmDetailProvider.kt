package org.alexz.app.imdb_app.viper.data.logic.film_detail

import io.reactivex.Single
import org.alexz.app.imdb_app.viper.data.entity.Film

interface FilmDetailProvider {
    fun provideFilmBy(imdbID: String): Single<Film>
}