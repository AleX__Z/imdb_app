package org.alexz.app.imdb_app.lang.arch.rxviper;

import org.reactivestreams.Subscriber;

import io.reactivex.subscribers.DisposableSubscriber;

class RxViperSubscriber<T> extends DisposableSubscriber<T> {
    private final Subscriber<T> subscriber;

    RxViperSubscriber(Subscriber<T> subscriber) {
        this.subscriber = subscriber;
    }

    @Override
    public void onNext(T t) {
        subscriber.onNext(t);
    }

    @Override
    public void onError(Throwable t) {
        subscriber.onError(t);
    }

    @Override
    public void onComplete() {
        subscriber.onComplete();
    }
}
