package org.alexz.app.imdb_app

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideApplication(app: App): Application = app

    @Provides
    fun provideApplicationContext(app: App): Context = app

}