package org.alexz.app.imdb_app

import dagger.BindsInstance
import dagger.Component
import org.alexz.app.imdb_app.lang.dagger.RxSchedulerModule
import org.alexz.app.imdb_app.viper.data.DataModule
import org.alexz.app.imdb_app.viper.domain.DomainModule
import org.alexz.app.imdb_app.viper.presentation.PresentationModule
import org.alexz.app.imdb_app.viper.presentation.logic.view.film_search.FilmSearchFragment
import org.alexz.app.imdb_app.viper.presentation.logic.view.film_detail.FilmDetailFragment

import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    RxSchedulerModule::class,
    DataModule::class,
    DomainModule::class,
    PresentationModule::class
])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        fun build(): AppComponent
    }

    fun inject(filmFragment: FilmSearchFragment)

    fun inject(filmDetailFragment: FilmDetailFragment)
}