package org.alexz.app.imdb_app.viper.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FilmPreview(
        @SerializedName("Title") val name: String,
        @SerializedName("imdbID") val imdbID: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString() as String,
            parcel.readString() as String)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(imdbID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilmPreview> {
        override fun createFromParcel(parcel: Parcel): FilmPreview {
            return FilmPreview(parcel)
        }

        override fun newArray(size: Int): Array<FilmPreview?> {
            return arrayOfNulls(size)
        }
    }
}