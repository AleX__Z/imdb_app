package org.alexz.app.imdb_app.viper.presentation.logic.view.film_search

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_film.*
import org.alexz.app.imdb_app.R
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.film_search.FilmSearchPresenter
import org.alexz.app.imdb_app.viper.presentation.logic.view.BaseAppFragment
import javax.inject.Inject

class FilmSearchFragment : BaseAppFragment<FilmSearchPresenter>(), FilmSearchView {
    private lateinit var filmSearchAdapter: FilmSearchAdapter

    @Inject
    lateinit var filmSearchPresenter: FilmSearchPresenter

    override fun getPresenter(): FilmSearchPresenter = filmSearchPresenter

    override fun inject() = daggerAppComponent.inject(this)

    override fun getLayoutRes(): Int = R.layout.fragment_film

    override fun initializationView() {
        searchButton.setOnClickListener {
            presenter.searchFilm(searchEditText.text.toString())
        }

        if (!::filmSearchAdapter.isInitialized) {
            filmSearchAdapter = FilmSearchAdapter(onClickItem = this::showFilmDetail)
        }

        searchFilmRecyclerView.layoutManager = LinearLayoutManager(activity)
        searchFilmRecyclerView.adapter = filmSearchAdapter
    }

    override fun showFilmDetail(filmPreview: FilmPreview) = presenter.showFilmDetail(filmPreview)

    override fun showFilmSearch(filmList: List<FilmPreview>) {
        filmSearchAdapter.filmSearchList = filmList
    }

    class FilmSearchAdapter(
            filmSearchList: List<FilmPreview> = emptyList(),
            private val onClickItem: (FilmPreview) -> Unit
    ) : RecyclerView.Adapter<FilmSearchAdapter.FilmViewHolder>() {
        var filmSearchList: List<FilmPreview> = filmSearchList
            set(value) {
                if (field != value) {
                    field = value

                    this.notifyDataSetChanged()
                }
            }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FilmViewHolder =
                FilmViewHolder(
                        LayoutInflater
                                .from(p0.context)
                                .inflate(android.R.layout.simple_list_item_1, p0, false),
                        onClickItem
                )

        override fun getItemCount(): Int = filmSearchList.size

        override fun onBindViewHolder(p0: FilmViewHolder, p1: Int) =
                p0.bind(filmSearchList[p1])


        class FilmViewHolder(
                itemView: View,
                private val onClickItem: (FilmPreview) -> Unit
        ) : RecyclerView.ViewHolder(itemView) {

            private val titleFilmTextView: TextView = itemView.findViewById(android.R.id.text1)

            fun bind(filmPreview: FilmPreview) {
                itemView.setOnClickListener { onClickItem(filmPreview) }

                titleFilmTextView.text = filmPreview.name
            }

        }
    }

}