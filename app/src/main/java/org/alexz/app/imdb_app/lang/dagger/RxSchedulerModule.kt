package org.alexz.app.imdb_app.lang.dagger

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.alexz.app.imdb_app.lang.dagger.Job
import org.alexz.app.imdb_app.lang.dagger.UI

@Module
class RxSchedulerModule {

    @Provides
    @Job
    @Singleton
    internal fun provideJobScheduler() = Schedulers.io()

    @Provides
    @UI
    @Singleton
    internal fun provideUIScheduler() = AndroidSchedulers.mainThread()

}
