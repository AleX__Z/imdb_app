package org.alexz.app.imdb_app.viper.domain

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import org.alexz.app.imdb_app.lang.dagger.Job
import org.alexz.app.imdb_app.lang.dagger.UI
import org.alexz.app.imdb_app.viper.data.logic.film_detail.FilmDetailProvider
import org.alexz.app.imdb_app.viper.data.logic.film_search.FilmSearchProvider
import org.alexz.app.imdb_app.viper.domain.logic.film_detail.FilmDetailInteractor
import org.alexz.app.imdb_app.viper.domain.logic.film_search.FilmSearchInteractor

@Module
class DomainModule {

    @Provides
    fun provideFilmSearchInteractor(
            @Job subscribeScheduler: Scheduler,
            @UI observeScheduler: Scheduler,
            filmSearchProvider: FilmSearchProvider
    ) =
            FilmSearchInteractor(subscribeScheduler, observeScheduler, filmSearchProvider)

    @Provides
    fun provideFilmDetailProvider(
            @Job subscribeScheduler: Scheduler,
            @UI observeScheduler: Scheduler,
            filmDetailProvider: FilmDetailProvider
    ) =
            FilmDetailInteractor(subscribeScheduler, observeScheduler, filmDetailProvider)
}