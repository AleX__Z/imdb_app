package org.alexz.app.imdb_app.viper.presentation

import dagger.Module
import dagger.Provides
import org.alexz.app.imdb_app.viper.domain.logic.film_detail.FilmDetailInteractor
import org.alexz.app.imdb_app.viper.domain.logic.film_search.FilmSearchInteractor
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.film_search.FilmSearchPresenter
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.film_detail.FilmDetailPresenter

@Module
class PresentationModule {

    @Provides
    fun provideFilmPresenter(filmSearchInteractor: FilmSearchInteractor) =
            FilmSearchPresenter(filmSearchInteractor)

    @Provides
    fun provideFilmDetailPresenter(filmDetailInteractor: FilmDetailInteractor) =
            FilmDetailPresenter(filmDetailInteractor)
}