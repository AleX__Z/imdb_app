package org.alexz.app.imdb_app.viper.presentation.logic.view

import android.widget.Toast
import org.alexz.app.imdb_app.MainActivity
import org.alexz.app.imdb_app.viper.presentation.logic.base.BaseFragment
import org.alexz.app.imdb_app.viper.presentation.logic.presenter.BaseAppPresenter

abstract class BaseAppFragment<T : BaseAppPresenter<*>> : BaseFragment<T>(), IAppView {

    protected fun clickBackButton() {
        if (fragmentManager != null && fragmentManager!!.backStackEntryCount > 0) {
            fragmentManager!!.popBackStack()
        } else {
            activity?.onBackPressed()
        }
    }

    override fun showError(message: String) =
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    override fun showProgress() = (activity as MainActivity).showProgress()

    override fun showContent() = (activity as MainActivity).showContent()

}