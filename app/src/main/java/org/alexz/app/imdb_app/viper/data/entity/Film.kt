package org.alexz.app.imdb_app.viper.data.entity

import com.google.gson.annotations.SerializedName

data class Film(
        @SerializedName("Title") val name: String,
        @SerializedName("Poster") val imageUrl: String,
        @SerializedName("imdbRating") val rating: Float,
        @SerializedName("Country") val country: String,
        @SerializedName("Director") val directorName: String,
        @SerializedName("Actors") val actorNames: String,
        @SerializedName("Plot") val description: String
)
