package org.alexz.app.imdb_app.viper.presentation.logic.view.film_search

import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.presentation.logic.view.IAppView

interface FilmSearchView : IAppView {
    fun showFilmDetail(filmPreview: FilmPreview)

    fun showFilmSearch(filmList: List<FilmPreview>)
}