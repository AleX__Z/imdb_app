package org.alexz.app.imdb_app.viper.domain.logic.film_detail

import io.reactivex.Flowable
import io.reactivex.Scheduler
import org.alexz.app.imdb_app.lang.arch.rxviper.Interactor
import org.alexz.app.imdb_app.viper.data.entity.Film
import org.alexz.app.imdb_app.viper.data.logic.film_detail.FilmDetailProvider

class FilmDetailInteractor(
        subscribeScheduler: Scheduler,
        observeScheduler: Scheduler,
        private val filmDetailProvider: FilmDetailProvider
) : Interactor<String, Film>(
        subscribeScheduler,
        observeScheduler) {

    override fun createFlowable(requestModel: String?): Flowable<Film> =
            filmDetailProvider.provideFilmBy(requestModel as String).toFlowable()
}