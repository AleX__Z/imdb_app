package org.alexz.app.imdb_app.viper.data.logic.film_search

import io.reactivex.Single
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview
import org.alexz.app.imdb_app.viper.data.logic.api.IMDBApiService

class FilmSearchProviderImpl : FilmSearchProvider {
    override fun getFilmSearchBy(word: String): Single<List<FilmPreview>> =
            IMDBApiService.service.getFilmSearchBy(word).map { it.listFilmPreview }
}