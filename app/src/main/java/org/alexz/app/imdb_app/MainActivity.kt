package org.alexz.app.imdb_app

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.alexz.app.imdb_app.viper.data.entity.FilmPreview


import org.alexz.app.imdb_app.viper.presentation.logic.base.BaseActivity
import org.alexz.app.imdb_app.viper.presentation.logic.base.BaseFragment

import org.alexz.app.imdb_app.viper.presentation.logic.router.IAppRouter
import org.alexz.app.imdb_app.viper.presentation.logic.view.film_search.FilmSearchFragment
import org.alexz.app.imdb_app.viper.presentation.logic.view.film_detail.FilmDetailFragment

class MainActivity : BaseActivity(), IAppRouter {
    override fun showSearchFilm() = showFragment(FilmSearchFragment())

    override fun showFilmDetail(filmPreview: FilmPreview) =
            showFragment(FilmDetailFragment.newInstance(filmPreview), true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStartRouter() = showSearchFilm()

    private fun showFragment(fragment: BaseFragment<*>, addBackStack: Boolean = false) =
            showFragment(R.id.contentView, fragment, addBackStack)

    fun showProgress() {
        if (viewSwitcher.displayedChild == 1)
            viewSwitcher.showPrevious()
    }

    fun showContent() {
        if (viewSwitcher.displayedChild == 0)
            viewSwitcher.showNext()
    }
}

