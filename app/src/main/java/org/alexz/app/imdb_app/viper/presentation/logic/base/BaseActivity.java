package org.alexz.app.imdb_app.viper.presentation.logic.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity implements IBaseRouter {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onStartRouter();
    }

    protected void showFragment(@IdRes int replaceId, BaseFragment fragment, boolean addBackStack) {
        FragmentManager.enableDebugLogging(true);
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(replaceId, fragment);
        if (addBackStack) tx.addToBackStack(fragment.getFragmentName());
        tx.commit();
    }
}