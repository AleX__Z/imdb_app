package org.alexz.app.imdb_app

import android.app.Application

class App : Application() {
    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
                .builder()
                .app(this)
                .build()

    }
}