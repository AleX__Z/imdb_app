package org.alexz.app.imdb_app.viper.presentation.logic.view

import org.alexz.app.imdb_app.lang.arch.rxviper.ViewCallbacks

interface IAppView : ViewCallbacks {
    fun showError(message: String)

    fun showProgress()

    fun showContent()
}
